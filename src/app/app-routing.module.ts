import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ManageAccountComponent } from './pages/manage-account/manage-account.component';
import { ManageChecklistComponent } from './pages/manage-checklist/manage-checklist.component';
import { ManageProjectComponent } from './pages/manage-project/manage-project.component';
import { ViewReportComponent } from './pages/view-report/view-report.component';

const homeRoutes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  {
    path: 'home',
    component: AuthLayoutComponent,
    children: [
      { path: '', component: LoginComponent }
    ]
  }
];

const secureHomeRoutes: Routes = [
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  },
  {
    path: 'dashboard',
    component: AdminLayoutComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'manage-account', component: ManageAccountComponent },
      { path: 'manage-checklist', component: ManageChecklistComponent },
      { path: 'manage-project', component: ManageProjectComponent },
      { path: 'view-report', component: ViewReportComponent }
    ]
  }
];

const routes: Routes = [
  {
      path: '',
      children: [
          ...homeRoutes,
          ...secureHomeRoutes,
          {
              path: '',
              component: AuthLayoutComponent
          }
      ]
  },
  {
    path: '**',
    redirectTo: '/home'
  }

];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
