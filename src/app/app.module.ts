import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { AuthLayoutComponent } from './layouts/auth-layout/auth-layout.component';
import { LoginComponent } from './pages/login/login.component';
import { HomeComponent } from './pages/home/home.component';
import { ManageAccountComponent } from './pages/manage-account/manage-account.component';
import { ManageChecklistComponent } from './pages/manage-checklist/manage-checklist.component';
import { ManageProjectComponent } from './pages/manage-project/manage-project.component';
import { ViewReportComponent } from './pages/view-report/view-report.component';

import { AppRoutingModule } from './app-routing.module';
import { ComponentsModule } from './components/components.module';

import {HttpModule} from "@angular/http";
import { AuthorizationService } from "./shared/authorization.service";
import { CognitoUtil } from "./service/cognito.service";

@NgModule({
  imports: [
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
    ComponentsModule,
    RouterModule,
    NgbModule,
    AppRoutingModule,
    HttpModule,
  ],
  declarations: [
    AppComponent,
    AdminLayoutComponent,
    AuthLayoutComponent,
    LoginComponent,
    HomeComponent,
    ManageAccountComponent,
    ManageChecklistComponent,
    ManageProjectComponent,
    ViewReportComponent
  ],
  providers: [
    AuthorizationService,
    CognitoUtil
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
