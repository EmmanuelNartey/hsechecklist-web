import { RouteInfo } from './sidebar.metadata';

export const ROUTES: RouteInfo[] = [
  {
    path: '/dashboard',
    title: 'Home',
    icon: 'mdi mdi-view-carousel',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '',
    title: 'Menu',
    icon: '',
    class: 'nav-small-cap',
    label: '',
    labelClass: '',
    extralink: true,
    submenu: []
  },
  {
    path: '/dashboard/manage-account',
    title: 'Manage Account',
    icon: 'mdi mdi-equal',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/manage-project',
    title: 'Manage Project',
    icon: 'mdi mdi-message-bulleted',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/manage-checklist',
    title: 'Manage Checklist',
    icon: 'mdi mdi-view-carousel',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  },
  {
    path: '/dashboard/view-report',
    title: 'View Report',
    icon: 'mdi mdi-arrange-bring-to-front',
    class: '',
    label: '',
    labelClass: '',
    extralink: false,
    submenu: []
  }
];
