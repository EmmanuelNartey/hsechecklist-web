import { Component, OnInit } from '@angular/core';
import { LoggedInCallback } from "../../service/cognito.service";
import { AuthorizationService } from "../../shared/authorization.service";
import { Router } from "@angular/router";

@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})

export class AdminLayoutComponent implements OnInit, LoggedInCallback {
  errorMessage: string = "";

  constructor(public router: Router, public auth: AuthorizationService) { 
      this.auth.isAuthenticated(this);
      console.log("SecureHomeComponent: constructor");
  }

  ngOnInit() {

  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
      if (!isLoggedIn) {
          this.router.navigate(['/home']);
      }
  }
}

