import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from "../../shared/authorization.service";
import { CognitoCallback, LoggedInCallback } from "../../service/cognito.service";
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements CognitoCallback, LoggedInCallback, OnInit { 
  email: string;
  password: string;
  rememberMe: boolean;
  errorMessage: string;

  constructor(private auth: AuthorizationService,
              private router: Router) {
  }

  ngOnInit() {
      this.errorMessage = null;
      console.log("Checking if the user is already authenticated. If so, then redirect to the secure site");
      this.auth.isAuthenticated(this);   
  }

  onLogin() {
      this.errorMessage = null;
      this.auth.authenticate(this.email, this.password, this);
  }

  cognitoCallback(message: string, result: any) {
      if (message != null) { //error
          this.errorMessage = message;
          console.log("result: " + this.errorMessage);
      } else { //success
          this.router.navigate(['/dashboard']);
      }
  }

  isLoggedIn(message: string, isLoggedIn: boolean) {
      if (isLoggedIn) {
          this.router.navigate(['/dashboard']);
      }
  }
}
