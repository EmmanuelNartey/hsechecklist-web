import { Component, OnInit } from '@angular/core';
import { AuthorizationService } from "../../shared/authorization.service";
import { Router } from '@angular/router';
import { Http, Headers } from "@angular/http";

@Component({
  selector: 'app-view-report',
  templateUrl: './view-report.component.html',
  styleUrls: ['./view-report.component.css']
})
export class ViewReportComponent implements OnInit{

  constructor(private auth: AuthorizationService,
              private router: Router,
              private http: Http) { 
  }

  ngOnInit() {
    
  }
}
