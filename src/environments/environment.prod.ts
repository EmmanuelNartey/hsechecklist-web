export const environment = {
  production: true,

  region: 'us-east-2',

  identityPoolId: 'us-east-2:fbe0340f-9ffc-4449-a935-bb6a6661fd53',
  userPoolId: 'us-east-2_lnIw8Ut6Y',
  clientId: '5mvrgc4bfjn881luld58m61kn',

  cognito_idp_endpoint: '',
  cognito_identity_endpoint: '',
  sts_endpoint: '',
  dynamodb_endpoint: '',
  s3_endpoint: ''
};
